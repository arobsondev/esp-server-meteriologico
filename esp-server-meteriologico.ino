#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>
#include <DHT.h>

AsyncWebServer servidor(80);

// Begin : Configuracoes de DHT
#define DHTPIN D3
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

float temp = 0.0;
float umid = 0.0;
// End : Configuracoes de DHT

// Begin : Variavel conteudo html
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <title>EspServer Metiorologico</title>
  </head>
  <body>
    <h2>Dados Meteriológicos</h2>
    <p>
      <i class="fas fa-thermometer-half" style="color:#cf4545;"></i> 
      <span class="dht-labels">Temperatura</span> 
      <span id="temperatura">%TEMPERATURA%</span>
      <sup>&deg;C</sup>
    </p>
    <p>
      <i class="fas fa-tint" style="color:#44b5ce;"></i> 
      <span>Umidade</span>
      <span id="umidade">%UMIDADE%</span>
      <sup>%</sup>
    </p>
  </body>
  <script>
    setInterval(function ( ) {
      var request = new XMLHttpRequest();

      request.open("GET", "/temperatura", true);
      request.send();
      request.onreadystatechange = function() {
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
          document.getElementById("temperatura").innerHTML = this.responseText;
        }
      };
    }, 5000 ) ;

    setInterval(function ( ) {
      var request = new XMLHttpRequest();

      request.open("GET", "/umidade", true);
      request.send();
      request.onreadystatechange = function() {
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
          document.getElementById("umidade").innerHTML = this.responseText;
        }
      };
    }, 5000 ) ;
  </script>
</html>
)rawliteral";
// End : Variavel conteudo html

String init_valores(const String& var){
  if(var == "TEMPERATURA"){
    return String(temp);
  }
  else if(var == "UMIDADE"){
    return String(umid);
  }
  return String();
}

void setup() {
  // Initialize the serial port
  Serial.begin(115200);

  // Inicializar sensor DHT11
  dht.begin();

  // Configurar wifi
  WiFi.begin("brisa-589876", "xa1qcq7j");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  // Respondendo requisicoes
  servidor.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/html", index_html, init_valores);
  });
  servidor.on("/temperatura", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(temp).c_str());
  });
  servidor.on("/umidade", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(umid).c_str());
  });

  // Inicializa o objeto http
  servidor.begin();
}

void loop() {
  // Exibindo informacao de qual ip esta servindo
  delay(5000);
  Serial.print("Meu endereco IP: ");
  Serial.println(WiFi.localIP());

  float aux_temp = dht.readTemperature();
  float aux_umid = dht.readHumidity();

  if(isnan(aux_temp) && isnan(aux_umid)) {
    Serial.println("Erro ao ler sensor DHT");
  } else {
    temp = aux_temp;
    umid = aux_umid;
  }
}
